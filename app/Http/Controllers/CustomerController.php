<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller 
{
	public function index()
	{
		return view('customer_index');
	}

	public function create()
	{
		return view('customer_create');
	}

	public function store(Request $request)
	{
		$customer		= new Customer();
		$customer->name		= $request->customer_name; 
		$customer->email	= $request->customer_email;
		$customer->save();
	}
}
?>

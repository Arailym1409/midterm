<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chocolife</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/responsive.css">
	<link href='//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet'/>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  </head>
  <body>
    <div class="wrapper">
      <div id="active-bar">
        <div class="container">
          <div class="row">
            <div class="active">
              <div class="col-md-6 col-xs-12 col-sm-6 links">
                <ul>
                  <li><a href="#">Choco<span class="food">Food</span></a></li>
                  <li><a href="#">Choco<span class="travel">Travel</span></a></li>
                  <li><a href="#">Lens<span class="Mark">Mark</span></a></li>
                  <li><a href="#"><span class="doctor">i</span>Doctor</a></li>
                </ul>
              </div>
              <div class="col-md-6 hidden-xs active-btn text-right">
                <ul>
                  <li><a href="#"><span>Регистрация</span></a></li>
                  <li><a href="#">Вход</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="help-bar">
        <div class="container">
          <div class="row row-table">
            <div class="help text-center">
              <div class="col-md-3 col-xs-12 region">
                <select class="region-btn" name="#">
                  <option value="#">Алматы</option>
                  <option value="#">Астана</option>
                </select>
              </div>
              <div class="col-md-3 col-xs-12 helped">
                <a href="#"><i class="far fa-lightbulb"></i> <span>Нужна помощь?</span></a>
              </div>
              <div class="col-md-3 col-xs-12 security text-center">
                <a href="#"><i class="fas fa-key"></i> <span>Защита покупателей</span></a>
              </div>
              <div class="col-md-3 col-xs-12 link-back">
                <a href="#"><i class="far fa-envelope"></i> <span>Обратная связь</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="main-bar">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <div class="mobile-button">
              <a href="#menu" class="menu-link hidden-md hidden-lg"><span></span></a>
            </div>
            <div class="menu">
              <ul>
                <li><a href="#">Все</a></li>
                <li><a href="#">Развлечение и Отдых</a></li>
                <li><a href="#">Красота и Здоровье</a></li>
                <li><img src="images/logo.png" alt=""></li>
                <li><a href="#">Спорт</a></li>
                <li><a href="#">Товары</a></li>
                <li><a href="#">Услуги</a></li>
                <li><a href="#">Еда</a></li>
                <li><a href="#">Туризм,отели</a></li>
              </ul>
              <div class="active-btn text-right hidden-md hidden-lg">
                <ul>
                  <li><a href="#"><span>Регистрация</span></a></li>
                  <li><a href="#">Вход</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      <div id="wall">
        <div class="wall-logo text-center">
          <img src="images/wall-logo.png" alt="">
          <div class="typed-text">
            <p><span class="typed-txt"></span></p>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <form action="" class="search text-center">
                <input type="text" class="search-input" placeholder="Найти среди 610 акций">
                <input type="button" class="search-btn" value="Найти">
              </form>
            </div>
          </div>
        </div>
      </div>
      <div id="category-block">
        <div class="container">
          <div class="row">
            <div class="col-md-12 products text-center">
              <ul>
                <li><span class="descr">
                  <h3>Отель Тау House</h3>
                  <h4>Проживание в лучших номерах на 1 сутки</h4>
                  <p><span>oт 36 000</span>  20 000 тг.</p>
                  <a href="detail.html"><button class="product-btn">Подробнее</button></a>
                </span><img src="https://static.chocolife.me/static/upload/images/deal/for_deal_page/37000/36354/310x240/1_201705310351614962219363044.jpg" alt=""></li>
                <li><span class="descr">
                  <h3>Фотостудия Victoriya</h3>
                  <h4>Love Story,семейная,детская и индивидуальные фотосессии</h4>
                  <p><span>oт 15 000</span>  7 500 тг.</p>
                  <button class="product-btn">Подробнее</button>
                </span><img src="https://static.chocolife.me/static/upload/images/deal/for_deal_page/35000/34617/310x240/9_2017112810114115118419414136.jpg" alt=""></li>
                <li><span class="descr">
                  <h3>Orhun Medical</h3>
                  <h4>Обследование МРТ</h4>
                  <p><span>oт 26 000</span>  11 900 тг.</p>
                  <button class="product-btn">Подробнее</button>
                </span><img src="https://static.chocolife.me/static/upload/images/deal/for_deal_page/40000/39555/310x240/4_201701310114914858467098399.JPG" alt=""></li>
                <li><span class="descr">
                  <h3>Cinema Towers 3D</h3>
                  <h4>Идеальное сочетание: 2 билета на любой сеанс + попкорн + 2 колы!</h4>
                  <p><span>oт 3 200</span>  2 240 тг.</p>
                  <button class="product-btn">Подробнее</button>
                </span><img src="https://static.chocolife.me/static/upload/images/deal/for_deal_page/40000/39421/310x240/8_2017110910112915102020090244.JPG" alt=""></li>
                <li><span class="descr">
                  <h3>Батутный Парк Гравитация</h3>
                  <h4>Вход в будние и выходные дни</h4>
                  <p><span>oт 1 500</span>  900 тг.</p>
                  <button class="product-btn">Подробнее</button>
                </span><img src="https://static.chocolife.me/static/upload/images/deal/for_deal_page/40000/39148/310x240/7_2017120509121315124435332843.jpg" alt=""></li>
                <li><span class="descr">
                  <h3>Квест "Заклятие"</h3>
                  <h4>От создателей квеста "Проклятие Анабель"</h4>
                  <p><span>oт 12 000</span>  6 000 тг.</p>
                  <button class="product-btn">Подробнее</button>
                </span><img src="https://static.chocolife.me/static/upload/images/deal/for_deal_page/41000/40133/310x240/1_201705051154814939617088439.JPG" alt=""></li>
                <li><span class="descr">
                  <h3>Автомойка на Кирова</h3>
                  <h4>Комплексная мойка автомобиля"</h4>
                  <p><span>oт 2 200</span>  1 000 тг.</p>
                  <button class="product-btn">Подробнее</button>
                </span><img src="https://static.chocolife.me/static/upload/images/deal/for_deal_page/40000/39674/310x240/5_201705300452414961406443273.JPG" alt=""></li>
                <li><span class="descr">
                  <h3>Караоке-Бар HAN BEL</h3>
                  <h4>Посещение караоке в дневное и вечернее время + новогоднее предложение для компании"</h4>
                  <p><span>oт 2 000</span>  200 тг.</p>
                  <button class="product-btn">Подробнее</button>
                </span><img src="https://static.chocolife.me/static/upload/images/deal/for_deal_page/39000/38621/310x240/1_201608080483414706523543562.jpg" alt=""></li>
                <li><span class="descr">
                  <h3>Территория красоты "Дядя Fёдор"</h3>
                  <h4>Горячее и холодное наращивание волос"</h4>
                  <p><span>oт 25 000</span>  6 750 тг.</p>
                  <button class="product-btn">Подробнее</button>
                </span><img src="https://static.chocolife.me/static/upload/images/deal/for_deal_page/41000/40108/310x240/5_201802020423515175676759336.jpg" alt=""></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
     <footer class="nb-footer">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="about">
	
	<div class="social-media">
		<ul class="list-inline">
			<li><a href="#"><i class="fab fa-facebook"></i></a></li>
			<li><a href="#" title=""><i class="fab fa-twitter"></i></a></li>
			<li><a href="#" title=""><i class="fab fa-google-plus"></i></a></li>
			<li><a href="#" title=""><i class="fab fa-linkedin"></i></a></li>
		</ul>
	</div>
</div>
</div>

<div class="col-md-3 col-sm-6">
<div class="footer-info-single">
	<h2 class="title">Help Center</h2>
	<ul class="list-unstyled">
		<li><a href="#" title=""><i class="fa fa-angle-double-right"></i> How to Pay</a></li>
		<li><a href="#" title=""><i class="fa fa-angle-double-right"></i> FAQ's</a></li>
	</ul>
</div>
</div>

<div class="col-md-3 col-sm-6">
<div class="footer-info-single">
	<h2 class="title">Customer information</h2>
	<ul class="list-unstyled">
		<li><a href="#" title=""><i class="fa fa-angle-double-right"></i> About Us</a></li>
		<li><a href="#" title=""><i class="fa fa-angle-double-right"></i> FAQ's</a></li>
	</ul>
</div>
</div>

<div class="col-md-3 col-sm-6">
<div class="footer-info-single">
	<h2 class="title">Security & privacy</h2>
	<ul class="list-unstyled">
		<li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Terms Of Use</a></li>
		<li><a href="#" title=""><i class="fa fa-angle-double-right"></i> Privacy Policy</a></li>
	</ul>
</div>
</div>

<div class="col-md-3 col-sm-6">
<div class="footer-info-single">
	<h2 class="title">Payment</h2>
	<p>Chocolife.me – купонный сервис №1 в Казахстане. В сервисе Chocolife.me представлены различные услуги со скидками до 90%.</p>
	
</div>
</div>
</div>
</div>

<section class="copyright">
<div class="container">
<div class="row">
<div class="col-sm-6">
<p>Copyright © 2017.Chocolife</p>
</div>
<div class="col-sm-6"></div>
</div>
</div>
</section>
</footer>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/typed.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>